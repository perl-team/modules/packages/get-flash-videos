Source: get-flash-videos
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>
Section: video
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: libhttp-cookies-perl,
                     libhttp-message-perl,
                     libio-socket-ssl-perl,
                     libmodule-find-perl,
                     libterm-progressbar-perl,
                     libterm-readkey-perl,
                     libtie-ixhash-perl,
                     liburi-perl,
                     libwww-mechanize-perl,
                     libwww-perl,
                     libxml-simple-perl,
                     perl
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/get-flash-videos
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/get-flash-videos.git
Homepage: https://github.com/monsieurvideo/get-flash-videos

Package: get-flash-videos
Architecture: all
Depends: libcrypt-blowfish-perl,
         libdata-amf-perl,
         libhtml-parser-perl,
         libhtml-tree-perl,
         libio-socket-ssl-perl,
         libmodule-find-perl,
         libterm-progressbar-perl,
         libterm-readkey-perl,
         libtie-ixhash-perl,
         liburi-perl,
         libwww-mechanize-perl,
         libwww-perl,
         rtmpdump,
         ${misc:Depends},
         ${perl:Depends}
Recommends: get-iplayer,
            libcrypt-rijndael-perl,
            liblwp-protocol-socks-perl,
            libxml-simple-perl
Suggests: mplayer
Description: video downloader for various Flash-based video hosting sites
 get-flash-videos download videos from various Flash-based video hosting
 sites, without having to use the Flash player. Handy for saving videos for
 watching offline, and means you don't have to keep upgrading Flash for sites
 that insist on a newer version of the player.
 .
 Includes support for the following sites/players (and more!):
 .
 YouTube, eHow, Brightcove (used by many sites like Channel 4, Daily
 Telegraph ...), BBC (news, etc), Metacafe, 5min, Google, fliqz,
 nicovideo, vimeo, Blip, Break, Collegehumor, Muzu, Sevenload, Megavideo,
 Wat.tv.
 .
 Also includes a 'generic' method which works on many other sites.
